<?php
// ----- Introduction: l'objet PDO -----

/* PDO (pour PHP Data Object) est un outil pour PHP permettant
d'acceder à une base de donnée et d'executer des requetes SQL.

PDO est une classe; on créé une donc une instance de cette classe avec la methode new PDO(),
qui renvoie un objet que l'on peut stocker dans une variable: */

$instance_pdo = new PDO("mysql:host=localhost;dbname=maBaseDeDonnee;charset=utf88", "root", "");
/* comme on peut le voir, new PDO prend 3 arguments:
- le premier est un string qui commence par le type de moteur de BDD, souvent mysql. Il est
suivi d'un ':', puis de la définition du host hebergeant la BDD (souvent localhost si on est
en local), du nom de la BDD, et du jeu de caractère qu'elle utilise -le tout séparé 
par des points-virgules
- le deuxieme argument est le login pour acceder à la BDD (par defaut root sous Windows)
- le troisieme argment est le mot de passe pour acceder à la BDD ( par defaut vide )
*/


// Une fois cet objet créé, il permet d'acceder à la BDD, et de faire des
// requetes SQL. Il y a pour cela plusieurs façon de proceder:

// -- Methode 1: Lancer directement la requete avec pdo->query 
// on utilise query, qui est une methode des instance de la classe PDO.
// Pour acceder à cette methode, on utiliser le symbole fleche '->'
// la methode query prend en paramètre une string, qui est la requete SQL à
// effectuer;
$requete = $instance_pdo->query("SELECT * FROM une_table_de_la_bdd");
// on peut aussi créer la requete SQL à part, la stocker dans une variable et
// la passer à query:
$texte_sql = "SELECT id, nom FROM une_table_de_la_bdd";
$autre_requete = $instance_pdo->query($texte_sql);

// -- Methode 2: preparer la requete avec des 'emplacement' pour les parametres,
// puis les 'remplir' avec des variables (solution plus securisée pour les
// informations sensibles)
// le nom des paramètres est libre, mais il faut les preceder par le caractère ':' 
$requete_preparee = $instance_pdo->prepare("SELECT * FROM table_quelconque WHERE login = :paramLog AND password = :paramPass"); 
// on cree un tableau associatif, donc les clé sont les noms des parametres sans les ':', et les valeurs sont les valeurs à envoyer 
$params = ["paramLog" => "SasukeRoxxor", "paramPass" => "mouette123"]
// puis on utilise la methode execute() de l'objet cree par prepare(), en
// passant comme argument le tableau de parametres
$requete_preparee->execute($params);

// ---- Reponse d'un objet PDO
// query() et execute() renvoient objet qui contient tous les resultats: vide pour une
// insertion, modification ou suppression; ou bien autant d'element (sous la forme de 
// tableau associatif) que de resultats trouvés pour un SELECT.

// Mettons ici que notre $autre_requete contienne maintenant les données
// [["id" => 1, "nom"=>"Sarah"], ["id" => 2, "prenom" => "Félix"]]
// on peut utiliser la methode fetchAll() de l'objet renvoyé par query ou execute()
// pour recuperer toutes les données sous forme de tableau:
$datas_recuperees = $autre_requete->fetchAll();
// alternativement, on peut recuperer le premier element avec fetch(). Cela supprime 
// en même temps cet element du resultat:
$premier_resultat = $autre_requete->fetch();// $cette variable vaut ["id" => 1, "prenom" => "Sarah"]
$deuxieme_resultat = $autre_requete->fetch();// $premier_resultat vaut ["id" => 2, "prenom" => "Félix"]


// ----- On corse encore un tout petit peu? -----

// Exemple de recuperation successive des elements grace à fetch() et une boucle:
//
$requete = $instance_pdo->query("SELECT * FROM une_table_de_la_bdd");
while ($element_actuel = $requete->fetch()) { // false si $requete->fetch() ne renvoie plus rien
  echo $data['id'] . "<br>";
}
// $element_actuel que l'on declare ici est ici une variable temporaire qui n'existe que
// dans le while, et dont la valeur est rendu egale au resultat de fetch(), 
// qui vide du même coup le premier resultat restant de l'objet $requete;
// while s'arretera donc quand tout

?>
