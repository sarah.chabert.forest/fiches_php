<?php

// ----- Bases de variables, strings et fonction echo -----

// en PHP, les variables sont déclarées avec un symbole $ 
$une_variable = "hello world"; // Assigne la valeur "hello world" à la variable $texte


// une des principales fonctions du php est de générer une page HTML
// la fonction 'echo' sert a generer du contenu pour la page
echo("Mouette!"); // ici, c'est juste du texte.

// Note: les parenthèses sont optionnelles pour la methode echo

echo "<br>"; // on peut egalement générer code HTML basique
echo $une_variable; // on peut imprimer le contenu d'une variable

/* jusqu'ici, notre page HTML contiendra:
Mouette!
<br>
hello_world

*/


// ----- On va plus en profondeur? (Replique) -----

// Dans un string, le caractère \ permet de signaler le caractère juste après
// comme devant être ecrit en 'dur'. Cela permet d'afficher les " plutot que
// le code les considère comme une fin de string.

$style_couleur = "style=\"color: orange; height: 25px;\" "; 
// $style_couleur a maintenant pour valeur:  style="color: orange"


// en PHP, les strings peuvent 'injecter' le contenu d'une variable dans leur contenu
// Pour le jargon, on appelle ça de l'interpolation de string
echo "<br>";
echo "<button $style_couleur>Ganbatte, Sarah-chan!</button>"; 
// La ligne precedente imprime <button style="color: orange;">Ganbatte, Sarah-chan!</button>



/* --- Des exercices, peut être? ---

A.1) Stocker un texte de quelques mots dans une nouvelle variable
  2) Quelle chaine faudra-t-il donner à la fonction echo pour imprimer une div
contenant ces quelques mots? (en utilisant l'interpolation)
  
B.1) declarer une nouvelle variable contenant une valeur au choix parmi "red", "orange" ou "green"
  2) en s'inspirant de la ligne 32, declarer une variable $styles pour stocker un string 
de style afin d'appliquer à la fois une margin-left au choix et la couleur choisie dans
la question B.1) (en utilisant l'interpolation)
  3) en s'inspirant de la ligne 40, imprimer un paragraphe ayant le style défini precedemment, et 
ayant comme texte les quelques mots choisis à la premiere etape de la question A.1)

*/

?>


