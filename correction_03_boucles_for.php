<?php 
// A.1)
for ($i = 5; $i < 10; $i++) {
  echo "$i ";
}
// A.2)
for ($i = 10; $i >= 0; $i -= 2) {
  echo "$i ";
}

//B.1)
$color = "red";
//B.2)
$styles = "style=\"color: $color;margin-left: 20px;\" "; // bien utiliser des \ pour garder les "
                                                        // a l'interieur du string
//B.3)
echo "<p $styles >$mots</p>";

?>
